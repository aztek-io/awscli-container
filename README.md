# README

## INFO

This is an automated build that will kick off at midnight (CDT) until the heat death of the universe or I stop paying my GitLab bill (whichever comes first).

```¯\_(ツ)_/¯```

This was created since Amazon does not maintain an AWS CLI image.  The amazonlinux image does not have the awscli, and installing the awscli image on the fly during CI jobs slows down my pipelines.

## HUB REPO

Here is the hub.docker.com repo:

https://hub.docker.com/r/aztek/awscli/
