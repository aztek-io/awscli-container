# hadolint ignore=DL3006
FROM alpine

LABEL maintainer="robert@aztek.io"

# hadolint ignore=DL3018
RUN apk add --no-cache \
        py3-pip \
        jq

ARG VERSION

RUN pip3 install "awscli==$VERSION"

ENTRYPOINT ["/usr/bin/aws"]
CMD ["--help"]
